using System.Collections.Generic;
using Services.Interfaces;

namespace Locator
{
    public static class ServiceLocator
    {
        private static readonly Dictionary<object, object> ServiceContainer = new Dictionary<object, object>();

        public static void RegisterService<T>(T service)
        {
            var type = typeof(T);
            if (ServiceContainer.ContainsKey(type))
            {
                ServiceContainer[type] = service;
            }
            else
            {
                ServiceContainer.Add(type, service);
            }
        }

        public static T GetService<T>() where T : class, IService
        {
            try
            {
                if (ServiceContainer.TryGetValue(typeof(T), out var service))
                {
                    if (service != null)
                    {
                        return (T) service;
                    }
                }
            }
            catch
            {
                throw new System.NotImplementedException("Can't find requested service");
            }

            return null;
        }
    }
}