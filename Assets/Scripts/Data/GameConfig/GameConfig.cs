using Data.PlayerData;
using UnityEngine;

namespace Data.GameConfig
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "ProjectsAssets/CreateGameConfig")]
    public class GameConfig : ScriptableObject
    {
        [Range(0f, 100f)] public int levelWeightDelta;
        [Range(0f, 100f)] public int minGameWeight;
        [Range(0f, 100f)] public int maxGameWeight;
        [Range(0f, 10)] public int levelWeightRange;
        [Range(0, 10)] public int circlesCount;
        public GameObject circlePrefab;
        public GameObject lastCirclePrefab;
        public float circlesSpawnRange;
        public PlayerPresenter playerPrefab;
        [Range(0, 1000f)] public float playerSpeed;
    }
}