using System;
using Input;
using Locator;
using UnityEngine;

namespace Data.PlayerData
{
    public class PlayerPresenter : EntityPresenter
    {
        [SerializeField] private PlayerView view;
        private IInputHandler input;

        public Transform Init(float speed, Action onCircleComplete, Action onCircleFault)
        {
            EntityView = view;
            view.onFault += onCircleFault;

            EntityModel = new PlayerModel {Speed = speed};

            view.onCircleComplete += delegate
            {
                if (!((PlayerModel) EntityModel).IsActive)
                {
                    onCircleComplete?.Invoke();
                }
            };

            EntityModel.OnModelUpdate += EntityView.Simulate;

            input = ServiceLocator.GetService<IInputHandler>();

            input.OnPressChange += ((PlayerModel) EntityModel).ChangeActive;
            return view.transform;
        }
    }
}