namespace Data.PlayerData
{
    public class PlayerModel : EntityModel
    {
        public bool IsActive { get; private set; }

        public override void Simulate()
        {
            OnModelUpdate?.Invoke(IsActive ? Speed : 0);
        }

        public void ChangeActive(bool active)
        {
            IsActive = active;
        }
    }
}