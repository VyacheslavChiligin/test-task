using System;
using UnityEngine;

namespace Data.PlayerData
{
    public class PlayerView : EntityView
    {
        public Action onCircleComplete;
        public Action onFault;

        public override void Simulate(float speed)
        {
            movingTransform.Rotate(Vector3.up, speed * Time.deltaTime, Space.World);
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Arrow"))
            {
                onCircleComplete?.Invoke();
            }
            else
            {
                onFault?.Invoke();
            }
        }
    }
}