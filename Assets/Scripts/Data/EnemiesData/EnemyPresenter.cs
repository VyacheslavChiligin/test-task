using UnityEngine;

namespace Data.EnemiesData
{
    public class EnemyPresenter : EntityPresenter
    {
        [SerializeField] private EnemyView view;

        public int EnemyWeight { get; private set; }

        public void Init(float speed, int weight)
        {
            EntityView = view;
            EntityModel = new EnemyModel {Speed = speed};
            EnemyWeight = weight;

            EntityModel.OnModelUpdate += EntityView.Simulate;
        }
    }
}