﻿using UnityEngine;

namespace Data.EnemiesData
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "ProjectsAssets/CreateEnemyConfig")]
    public class EnemyConfig : ScriptableObject
    {
        public int enemyWeight;
        [Range(0f, 200f)] public float enemySpeed;
        public EnemyPresenter enemyPresenter;
    }
}