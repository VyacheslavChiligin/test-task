using UnityEngine;

namespace Data.EnemiesData
{
    public class EnemyView : EntityView
    {
        public override void Simulate(float speed)
        {
            movingTransform.Rotate(Vector3.up, speed * Time.deltaTime, Space.World);
        }
    }
}