using System;
using System.Collections.Generic;
using Data.EnemiesData;

namespace Data.Level
{
    [Serializable]
    public class LevelData
    {
        public Dictionary<int, EnemyPresenter[]> enemies;
    }
}