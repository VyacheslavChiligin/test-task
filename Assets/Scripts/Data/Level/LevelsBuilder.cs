using System.Collections.Generic;
using Data.EnemiesData;
using Locator;
using Services.Interfaces;
using Random = UnityEngine.Random;

namespace Data.Level
{
    public static class LevelBuilder
    {
        public static LevelData BuildLevel(int circlesCount, int lvlMinWeight, int lvlMaxWeight)
        {
            LevelData level = new LevelData
            {
                enemies = new Dictionary<int, EnemyPresenter[]>()
            };

            level.enemies[0] = new EnemyPresenter[0];

            for (int i = 1; i < circlesCount; i++)
            {
                level.enemies[i] = BuildCircle(lvlMinWeight, lvlMaxWeight);
            }

            return level;
        }

        private static EnemyPresenter[] BuildCircle(int minWeight, int maxWeight)
        {
            IEnemyPool enemyPool = ServiceLocator.GetService<IEnemyPool>();
            List<EnemyPresenter> enemies = new List<EnemyPresenter>();

            for (int i = 0; i < maxWeight;)
            {
                int randomWeight = Random.Range(0, maxWeight + 1 - i);

                if (enemyPool.CanCreate(randomWeight))
                {
                    enemies.Add(enemyPool.CreateEnemy(randomWeight));
                    i += randomWeight;
                }

                if (i >= minWeight)
                {
                    return enemies.ToArray();
                }
            }

            return enemies.ToArray();
        }
    }
}