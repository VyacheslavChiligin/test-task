using UnityEngine;

namespace Data
{
    public class EntityPresenter : MonoBehaviour
    {
        protected EntityView EntityView;
        protected EntityModel EntityModel;

        public virtual void Simulate()
        {
            EntityModel.Simulate();
        }
    }
}