using System;

namespace Data
{
    public class EntityModel
    {
        public float Speed;
        public Action<float> OnModelUpdate;

        public virtual void Simulate()
        {
            OnModelUpdate?.Invoke(Speed);
        }
    }
}