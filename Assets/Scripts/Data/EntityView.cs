using UnityEngine;

namespace Data
{
    public abstract class EntityView : MonoBehaviour
    {
        public Transform movingTransform;
        public abstract void Simulate(float speed);
    }
}