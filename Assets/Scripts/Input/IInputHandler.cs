using System;
using Services.Interfaces;

namespace Input
{
    public interface IInputHandler : IService
    {
        Action<bool> OnPressChange { get; set; }
    }
}