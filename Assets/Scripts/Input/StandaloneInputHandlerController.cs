using System;
using UnityEngine;

namespace Input
{
    public class StandaloneInputHandlerController : MonoBehaviour, IInputHandler
    {
        [SerializeField] private KeyCode keyCode;

        public Action<bool> OnPressChange { get; set; }

        private void Update()
        {
            if (UnityEngine.Input.GetKeyUp(keyCode))
            {
                OnPressChange?.Invoke(false);
            }

            if (UnityEngine.Input.GetKeyDown(keyCode))
            {
                OnPressChange?.Invoke(true);
            }
        }
    }
}