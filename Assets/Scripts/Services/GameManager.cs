using System;
using System.Collections.Generic;
using Data.EnemiesData;
using Data.GameConfig;
using Data.Level;
using Data.PlayerData;
using Locator;
using Services.Interfaces;
using UnityEngine;
using UserInterface;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

namespace Services
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Vector3 circlesSpawnStart;
        [SerializeField] private Vector3 spawnStart;

        private GameConfig gameConfig;
        private LevelData currentLevel;
        private bool levelStarted;
        private PlayerPresenter player;
        private int completedCircles = 0;
        private IPopupView popupView;
        private IProgressView progressView;
        private IEnemyPool enemyPool;
        private int currentMaxWeight;
        private int currentMinWeight;
        private int lvlDelta;
        private int curLevelIndex;
        private float distance;
        private Transform playerMovingTransform;
        private Vector3 playerStartPosition;

        public void Init(GameConfig config)
        {
            gameConfig = config;
            popupView = ServiceLocator.GetService<IPopupView>();
            progressView = ServiceLocator.GetService<IProgressView>();
            enemyPool = ServiceLocator.GetService<IEnemyPool>();

            popupView.OnNextLevel += StartLevel;
            popupView.OnRetryLevel += RetryLevel;
            lvlDelta = gameConfig.levelWeightRange;
            currentMaxWeight = gameConfig.maxGameWeight;
            currentMinWeight = gameConfig.minGameWeight;

            player = Instantiate(gameConfig.playerPrefab, spawnStart, Quaternion.identity);
            playerMovingTransform = player.Init(config.playerSpeed, OnCircleComplete, OnCircleFault);
            ServiceLocator.GetService<ICameraService>().SetFollowTransform(playerMovingTransform);

            for (int i = 0; i < gameConfig.circlesCount; i++)
            {
                var circle = i == gameConfig.circlesCount - 1 ? gameConfig.lastCirclePrefab : gameConfig.circlePrefab;
                Instantiate(circle, circlesSpawnStart + Vector3.forward * gameConfig.circlesSpawnRange * i, Quaternion.identity);
            }

            playerStartPosition = playerMovingTransform.position;
            distance = Vector3.Distance(playerStartPosition,
                spawnStart + Vector3.forward * gameConfig.circlesSpawnRange * (gameConfig.circlesCount - 1) + Vector3.forward * 1f);
        }

        public void StartLevel()
        {
            currentLevel = LevelBuilder.BuildLevel(gameConfig.circlesCount,
                Mathf.Clamp(currentMinWeight - lvlDelta, 0, currentMinWeight - lvlDelta), currentMinWeight);

            for (int i = 0; i < gameConfig.circlesCount; i++)
            {
                SpawnEnemiesOnCircle(i, currentLevel.enemies[i]);
            }

            progressView.ChangeActive(true);
            progressView.UpdateLevel(curLevelIndex);

            player.gameObject.SetActive(true);
            levelStarted = true;
        }

        private void Update()
        {
            if (levelStarted)
            {
                progressView.UpdateProgress(Vector3.Distance(playerStartPosition, playerMovingTransform.position) /
                                            distance);
                SimulateLevel();
            }
        }

        private void RetryLevel()
        {
            levelStarted = true;
            progressView.ChangeActive(true);
            player.gameObject.SetActive(true);
        }

        private void SimulateLevel()
        {
            if (currentLevel == null) return;

            player.Simulate();
            foreach (KeyValuePair<int, EnemyPresenter[]> levelEnemy in currentLevel.enemies)
            {
                foreach (EnemyPresenter enemyPresenter in levelEnemy.Value)
                {
                    enemyPresenter.Simulate();
                }
            }
        }

        private void SpawnEnemiesOnCircle(int order, EnemyPresenter[] enemies)
        {
            float spawnRange = gameConfig.circlesSpawnRange;

            foreach (EnemyPresenter enemy in enemies)
            {
                enemy.transform.position = spawnStart + Vector3.forward * spawnRange * order;
                enemy.transform.rotation = Quaternion.Euler(0, Random.Range(40, 140f) * (float) Math.Pow(-1, Random.Range(1, 3)), 0);
            }
        }

        private void OnCircleComplete()
        {
            completedCircles++;

            if (completedCircles == currentLevel.enemies.Count)
            {
                ClearLevel();
                curLevelIndex++;
                currentMinWeight = Mathf.Clamp(currentMinWeight + gameConfig.levelWeightDelta, 0, currentMaxWeight);
                popupView.ChangeActive(PopupState.Win);
                progressView.ChangeActive(false);
            }
            else
            {
                Transform playerTransform = player.transform;
                playerTransform.rotation = Quaternion.identity;
                playerTransform.position += Vector3.forward * gameConfig.circlesSpawnRange;
            }
        }

        private void OnCircleFault()
        {
            completedCircles = 0;
            levelStarted = false;
            popupView.ChangeActive(PopupState.Lose);
            progressView.ChangeActive(false);
            ResetPlayer();
        }

        private void ClearLevel()
        {
            completedCircles = 0;
            levelStarted = false;
            enemyPool.ReturnAllToPool();
            ResetPlayer();
        }

        private void ResetPlayer()
        {
            player.gameObject.SetActive(false);
            Transform playerTransform = player.transform;
            playerTransform.position = spawnStart;
            playerTransform.rotation = Quaternion.identity;
        }
    }
}