using Data.EnemiesData;

namespace Services.Interfaces
{
    public interface IEnemyPool : IService
    {
        bool CanCreate(int weight);
        EnemyPresenter CreateEnemy(int enemyWeight);
        void ReturnAllToPool();
    }
}