using Cinemachine;
using UnityEngine;

namespace Services.Interfaces
{
    public class CameraService : MonoBehaviour, ICameraService
    {
        private CinemachineVirtualCamera cinemachineCamera;

        private void Awake()
        {
            cinemachineCamera = GetComponent<CinemachineVirtualCamera>();
        }

        public void SetFollowTransform(Transform followTransform)
        {
            cinemachineCamera.Follow = followTransform;
        }
    }
}