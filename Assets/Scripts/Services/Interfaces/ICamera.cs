using UnityEngine;

namespace Services.Interfaces
{
    public interface ICameraService : IService
    {
        void SetFollowTransform(Transform followTransform);
    }
}