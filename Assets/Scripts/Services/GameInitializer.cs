using Data.GameConfig;
using Input;
using Locator;
using Services.Interfaces;
using UnityEngine;
using UserInterface;

namespace Services
{
    public class GameInitializer : MonoBehaviour
    {
        [SerializeField] private GameConfig gameConfig;
        [SerializeField] private StandaloneInputHandlerController standaloneInput;
        [SerializeField] private PopupView popupView;
        [SerializeField] private GameManager gameManager;
        [SerializeField] private EnemyPool enemyPool;
        [SerializeField] private CameraService virtualCamera;
        [SerializeField] private ProgressView progressView;

        private void Awake()
        {
            InitCommonServices();

            switch (Application.platform)
            {
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    InitStandaloneServices();
                    break;
            }
        }

        private void Start()
        {
            gameManager.Init(gameConfig);
            gameManager.StartLevel();
        }

        private void InitCommonServices()
        {
            ServiceLocator.RegisterService<IEnemyPool>(enemyPool);
            ServiceLocator.RegisterService<IPopupView>(popupView);
            ServiceLocator.RegisterService<ICameraService>(virtualCamera);
            ServiceLocator.RegisterService<IProgressView>(progressView);
        }

        private void InitStandaloneServices()
        {
            ServiceLocator.RegisterService<IInputHandler>(standaloneInput);
        }
    }
}