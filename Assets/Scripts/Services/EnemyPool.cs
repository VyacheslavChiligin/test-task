using System.Collections.Generic;
using Data.EnemiesData;
using Extensions;
using Services.Interfaces;
using UnityEngine;

namespace Services
{
    public class EnemyPool : MonoBehaviour, IEnemyPool
    {
        [SerializeField] private List<EnemyConfig> enemies;
        [SerializeField] private int enemyCount;

        private IDictionary<int, List<EnemyPresenter>> weightEnemiesMap;
        private List<EnemyPresenter> pooledObjects;

        public void Awake()
        {
            weightEnemiesMap = new Dictionary<int, List<EnemyPresenter>>();
            pooledObjects = new List<EnemyPresenter>();

            foreach (EnemyConfig enemyConfig in enemies)
            {
                List<EnemyPresenter> spawnEnemies = new List<EnemyPresenter>();

                for (int i = 0; i < enemyCount; i++)
                {
                    EnemyPresenter enemyPresenter = Instantiate(enemyConfig.enemyPresenter, transform);
                    enemyPresenter.Init(enemyConfig.enemySpeed, enemyConfig.enemyWeight);
                    enemyPresenter.gameObject.SetActive(false);
                    spawnEnemies.Add(enemyPresenter);
                }

                if (weightEnemiesMap.ContainsKey(enemyConfig.enemyWeight))
                {
                    weightEnemiesMap[enemyConfig.enemyWeight].AddRange(spawnEnemies);
                }
                else
                {
                    weightEnemiesMap.Add(enemyConfig.enemyWeight, spawnEnemies);
                }

                weightEnemiesMap[enemyConfig.enemyWeight].Shuffle();
            }
        }

        public bool CanCreate(int weight)
        {
            return weightEnemiesMap.ContainsKey(weight);
        }

        public EnemyPresenter CreateEnemy(int enemyWeight)
        {
            EnemyPresenter enemyPresenter = weightEnemiesMap[enemyWeight][Random.Range(0, weightEnemiesMap[enemyWeight].Count)];
            weightEnemiesMap[enemyWeight].Remove(enemyPresenter);
            pooledObjects.Add(enemyPresenter);
            enemyPresenter.gameObject.SetActive(true);

            return enemyPresenter;
        }

        public void ReturnAllToPool()
        {
            foreach (EnemyPresenter enemyPresenter in pooledObjects)
            {
                weightEnemiesMap[enemyPresenter.EnemyWeight].Add(enemyPresenter);
                enemyPresenter.gameObject.SetActive(false);
            }

            pooledObjects.Clear();
        }
    }
}