using System;
using Services.Interfaces;

namespace UserInterface
{
    public interface IPopupView : IService
    {
        void ChangeActive(PopupState state);
        Action OnNextLevel { get; set; }
        Action OnRetryLevel { get; set; }
    }
}