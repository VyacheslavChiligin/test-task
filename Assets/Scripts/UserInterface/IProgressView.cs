using Services.Interfaces;

namespace UserInterface
{
    public interface IProgressView : IService
    {
        void UpdateProgress(float progress);
        void UpdateLevel(int level);
        void ChangeActive(bool active);
    }
}