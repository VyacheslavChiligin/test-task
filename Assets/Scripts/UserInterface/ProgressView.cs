using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UserInterface
{
    public class ProgressView : MonoBehaviour, IProgressView
    {
        [SerializeField] private Slider progress;
        [SerializeField] private TMP_Text level;

        public void UpdateProgress(float progress)
        {
            this.progress.value = progress;
        }

        public void UpdateLevel(int level)
        {
            this.level.text = level.ToString();
        }

        public void ChangeActive(bool active)
        {
            progress.gameObject.SetActive(active);
            level.gameObject.SetActive(active);
        }
    }
}