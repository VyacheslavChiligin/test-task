using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UserInterface
{
    public class PopupView : MonoBehaviour, IPopupView
    {
        public Action OnNextLevel { get; set; }
        public Action OnRetryLevel { get; set; }

        [SerializeField] private TMP_Text labelText;
        [SerializeField] private TMP_Text buttonText;
        [SerializeField] private Button button;
        [SerializeField] private string winText;
        [SerializeField] private string loseText;
        [SerializeField] private string winBtnText;
        [SerializeField] private string loseBtnText;

        private PopupState popupState;

        public void ChangeActive(PopupState state)
        {
            popupState = state;
            switch (state)
            {
                case PopupState.Off:
                    gameObject.SetActive(false);
                    break;
                case PopupState.Win:
                    gameObject.SetActive(true);
                    labelText.text = winText;
                    buttonText.text = winBtnText;
                    break;
                case PopupState.Lose:
                    gameObject.SetActive(true);
                    labelText.text = loseText;
                    buttonText.text = loseBtnText;
                    break;
            }
        }

        private void Awake()
        {
            if (button != null)
            {
                button.onClick.AddListener(delegate
                {
                    if (popupState == PopupState.Win)
                    {
                        OnNextLevel?.Invoke();
                    }
                    else
                    {
                        OnRetryLevel?.Invoke();
                    }

                    ChangeActive(PopupState.Off);
                });
            }
        }
    }

    public enum PopupState
    {
        Off,
        Win,
        Lose
    }
}